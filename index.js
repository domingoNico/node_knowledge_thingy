var express = require('express');
var app = express();
var http = require('http').Server(app);

/*incantations for session and form handling*/
var expressSession = require('express-session');
var MongoStore = require('connect-mongo')(expressSession);
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var passport = require('passport');
/*incantations for session and form handling*/
var mongocon = require('./persistence/mongocon');
var hbs = require('hbs');
var routes = require('./lib/routes');




app.use(cookieParser());
app.use(expressSession({
  secret: 'helloHi',
  store: new MongoStore({
    url: mongocon.connection
  })
}));
app.use(bodyParser({ extended: true }));



//init passport
app.use(passport.initialize());
app.use(passport.session());


app.set('view engine', 'hbs');
hbs.registerPartials(__dirname + '/views/partials');


routes(app);

http.listen(5000, function () {
  console.log('listen 5000');
});
