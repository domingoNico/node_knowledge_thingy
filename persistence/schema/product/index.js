var mongocon = require('../../mongocon');

var mongoose = mongocon.mongoose;


var productSchema = new mongoose.Schema({
  name: String,
  price: Number
});

module.exports = mongoose.model('Product', productSchema);
