var mongocon = require('../../mongocon');

var mongoose = mongocon.mongoose;


var userSchema = new mongoose.Schema({
  username: String,
  password: String
});


userSchema.methods.validatePassword = function (password) {
  return this.password === password;
};



module.exports = mongoose.model('User', userSchema);
