var User = require('../schema/user');
var Product = require('../schema/product');


module.exports = {
  Product: Product,
  User: User
};
