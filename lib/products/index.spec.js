var sinon = require('sinon');
var chai = require('chai');
var assert = require('assert');
var proxyquire = require('proxyquire');

describe('get products test', function () {

  var productServiceStub = sinon.stub();

  var lib = proxyquire('./index.js', { './productService': productServiceStub });



  it('call callback w/ err when error', function () {
    var callbackStub = sinon.stub();
    var err = { 'a': 'a' };

    productServiceStub.yields(err);
    lib.getProds(callbackStub);
    assert.strictEqual(true, callbackStub.calledWith(err));

  });


  it('call callback w/ data when success', function () {
    var callbackStub = sinon.stub();
    var data = ['0', '1'];
    var expectedResult = ['asd' + data[0], 'dsfsd' + data[1]];


    productServiceStub.yields(null, data);
    lib.getProds(callbackStub);
    console.log(callbackStub.args);
    assert.strictEqual(expectedResult[0], callbackStub.args[0][1][0]);

  });


});
