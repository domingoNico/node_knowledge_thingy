function _getProds(callback) {

  setTimeout(function () {

    var rtn = [];

    rtn.push({ name: 'apple', price: '30' });
    rtn.push({ name: 'orange', price: '40' });
    rtn.push({ name: 'banana', price: '50' });
    rtn.push({ name: 'durian', price: '60' });
    rtn.push({ name: 'something nice', price: '70' });
    rtn.push({ name: 'cake', price: '90' });
    rtn.push({ name: 'marlboro', price: '100' });

    return callback(null, rtn);

  }, 5000);

}


module.exports = _getProds;
