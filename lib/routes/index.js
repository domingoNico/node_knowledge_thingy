var persistenceProvider = require('../../persistence/provider');
var User = persistenceProvider.User;
var auth = require('../auth');
var passport = require('passport');


module.exports = function (app) {

  app.get('/login', function (req, res) {
    return res.render('index');
  });


  app.post('/login', function (req, res, next) {
    passport.authenticate('local-auth', function (err, user, info) {
      if (err) {
        return next(err);
      }
      if (!user) {
        return res.send(info);
      }
      req.logIn(user, function (err) {
        if (err) {
          return next(err);
        }
        return res.redirect('/users/' + user.username);
      });
    })(req, res, next);


  });

  app.get('/users/:username', auth.checkIfLoggedIn, function (req, res) {

    User.find({ username: req.params.username }, function (err, data) {
      res.send(data);
    });
  });



  app.get('/signup', function (req, res) {
    User.find({}, function (error, docs) {
      if (error) {
        console.log(error);
      } else {
        res.render('signup', {
          title: 'Signup page',
          users: docs
        });
      }
    });
  }).post('/signup', function (req, res) {
    var username = req.body.username;
    var password = req.body.password;

    var newUser = new User({
      username: username,
      password: password
    });

    newUser.save(function (err) {
      if (err) {
        return res.send(err);
      }

      res.redirect('/signup');
    });
  }).post('/delete', function (req, res) {
    User.remove({
        _id: req.body.id
      },
      function (error) {
        if (error) {
          return res.send(error);
        }

        return res.redirect('/signup');
      }
    );
  });
};
