var passport = require('passport');
var LocalStrategy = require('passport-local');
var User = require('../../persistence/provider').User;
passport.serializeUser(function (user, done) {
  return done(null, user._id);
});


passport.deserializeUser(function (id, done) {
  User.findById(id, function (err, user) {
    return done(err, user);
  });
});


passport.use('local-auth', new LocalStrategy(function (username, password, done) {
  User.findOne({ username: username }, function (err, user) {
    if (err) {
      return done(err);
    }

    if (!user) {
      return done(null, false, { message: 'Incorrect username' });
    }

    if (!user.validatePassword(password)) {
      return done(null, false, { message: 'Incorrect password' });
    }

    return done(null, user);

  });
}));



var auth = {

  checkIfLoggedIn: function (req, res, next) {
    if (req.isAuthenticated()) {
      return next();
    }
    res.redirect('/login');
  }

};

module.exports = auth;
